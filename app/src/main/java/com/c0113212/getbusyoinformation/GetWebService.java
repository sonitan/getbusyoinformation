package com.c0113212.getbusyoinformation;

/**
 * Created by Kmac on 16/01/07.
 */

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

public class GetWebService extends Activity {
    private static final String FILE_NAME = "FileSampleFile";
    CheckWord ckw01 = new CheckWord();
    int ckw02 = 0;
    int[] n = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0,0};
    String remove;
    int writecount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_service);
        ScrollView scrollView = new ScrollView(this);
        setContentView(scrollView);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        // ScrollView に LinearLayout を追加
        scrollView.addView(linearLayout);

        //GetWebActivityからの情報取得
        Bundle extra = getIntent().getExtras();
        String geturl = extra.getString("GETURL01");
        String geturl02 = extra.getString("GETURL02");
        String geturl03 = extra.getString("GETURL03");
        String geturl04 = extra.getString("GETURL04");
        String geturl05 = extra.getString("GETURL05");
        String name01 = extra.getString("GETNAME01");
        String name02 = extra.getString("GETNAME02");
        String name03 = extra.getString("GETNAME03");
        String name04 = extra.getString("GETNAME04");
        String name05 = extra.getString("GETNAME05");
        String check="";

        try {

            StrictMode.setThreadPolicy(new Builder().permitAll().build());
            String TagetUrl01 = geturl;//URLを変数へ
            String TagetUrl02 = geturl02;//URLを変数へ
            String TagetUrl03 = geturl03;//URLを変数へ
            String TagetUrl04 = geturl04;//URLを変数へ
            String TagetUrl05 = geturl05;//URLを変数へ

            String strLine;
            URL url01 = new URL(TagetUrl01);
            URL url02 = new URL(TagetUrl02);
            URL url03 = new URL(TagetUrl03);
            URL url04 = new URL(TagetUrl04);
            URL url05 = new URL(TagetUrl05);
            Object content01 = url01.getContent();
            Object content02 = url02.getContent();
            Object content03 = url03.getContent();
            Object content04 = url04.getContent();
            Object content05 = url05.getContent();

            //1人目
            if ((content01 instanceof InputStream))
            {
                BufferedReader bf = new BufferedReader(new InputStreamReader
                        ((InputStream) content01, "utf-8"));//文字コード

                while ((strLine = bf.readLine()) != null) {
                    Log.d("ログ", strLine); //ソースコード確認
                    remove = HtmlTagRemover1(strLine);

                    if (remove != "\n") {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));

                            if (ckw02 == 0)
                            {
                                if(name01.equals(check)){
                                    out.write("------------取得失敗--------------");
                                    out.newLine();
                                    out.write("武将名を記述してくだい");
                                    out.newLine();
                                    out.write("---------------------------------");
                                    out.newLine();
                                    out.newLine();
                                }
                                else {
                                    out.write("------------" + name01 + "の武将情報--------------");
                                }
                                out.newLine();
                                ckw02 = 1;
                            }

                            if (writecount == 1)
                            {
                                out.write("\t\t-" + remove.toString());
                                out.newLine();
                                writecount = 0;
                                if ((n[0] == 1) && (n[1] == 1) && (n[2] == 1) && (n[3] == 1) && (n[4] == 1)
                                        && (n[5] == 1)&& (n[6] == 1)&& (n[7] == 1)&& (n[8] == 1)&& (n[9] == 1)&& (n[10] == 0))
                                {
                                    out.write("--------------ここまで----------------");
                                    out.newLine();
                                    out.newLine();
                                    n[10] = 1;
                                }
                            }
                            if ((remove.indexOf("凡例") != -1) && (n[0] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[0] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("時代") != -1) && (n[1] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[1] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("生誕") != -1) && (n[2] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[2] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("死没") != -1) && (n[3] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[3] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("改名") != -1) && (n[4] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[4] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("官位") != -1) && (n[5] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[5] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("主君") != -1) && (n[6] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[6] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("氏族") != -1) && (n[7] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[7] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("父母") != -1) && (n[8] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[8] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("兄弟") != -1) && (n[9] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[9] = 1;
                                writecount += 1;
                            }

                            out.close();
                        }
                        catch (Exception e) {}
                    }
                }
                ckw01.CheckWord();
            }//ここまで

            //2人目
            if ((content02 instanceof InputStream))
            {
                BufferedReader bf = new BufferedReader(new InputStreamReader
                        ((InputStream) content02, "utf-8"));//文字コード

                while ((strLine = bf.readLine()) != null) {
                    Log.d("ログ", strLine); //ソースコード確認
                    remove = HtmlTagRemover1(strLine);

                    if (remove != "\n") {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));
                            if (ckw02 == 0)
                            {
                                if(name02.equals(check)){
                                    out.write("------------取得失敗--------------");
                                    out.newLine();
                                    out.write("武将名を記述してくだい");
                                    out.newLine();
                                    out.write("---------------------------------");
                                    out.newLine();
                                    out.newLine();
                                }
                                else {
                                    out.write("------------" + name02 + "の武将情報--------------");
                                }
                                out.newLine();
                                ckw02 = 1;
                            }

                            if (writecount == 1)
                            {
                                out.write("\t\t-" + remove.toString());
                                out.newLine();
                                writecount = 0;
                                if ((n[0] == 1) && (n[1] == 1) && (n[2] == 1) && (n[3] == 1) && (n[4] == 1)
                                        && (n[5] == 1)&& (n[6] == 1)&& (n[7] == 1)&& (n[8] == 1)&& (n[9] == 1)&& (n[10] == 0))
                                {
                                    out.write("--------------ここまで----------------");
                                    out.newLine();
                                    out.newLine();
                                    n[10] = 1;
                                }
                            }
                            if ((this.remove.indexOf("凡例") != -1) && (this.n[0] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[0] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("時代") != -1) && (n[1] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[1] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("生誕") != -1) && (n[2] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[2] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("死没") != -1) && (n[3] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[3] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("改名") != -1) && (n[4] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[4] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("官位") != -1) && (n[5] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[5] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("主君") != -1) && (n[6] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[6] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("氏族") != -1) && (n[7] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[7] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("父母") != -1) && (n[8] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[8] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("兄弟") != -1) && (n[9] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[9] = 1;
                                writecount += 1;
                            }

                            out.close();
                        }
                        catch (Exception e) {}
                    }
                }
                ckw01.CheckWord();
            }//ここまで

            //3人目
            if ((content03 instanceof InputStream))
            {
                BufferedReader bf = new BufferedReader(new InputStreamReader
                        ((InputStream) content03, "utf-8"));//文字コード

                while ((strLine = bf.readLine()) != null) {
                    Log.d("ログ", strLine); //ソースコード確認
                    remove = HtmlTagRemover1(strLine);

                    if (remove != "\n") {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));
                            if (ckw02 == 0)
                            {
                                if(name03.equals(check)){
                                    out.write("------------取得失敗--------------");
                                    out.newLine();
                                    out.write("武将名を記述してくだい");
                                    out.newLine();
                                    out.write("---------------------------------");
                                    out.newLine();
                                    out.newLine();
                                }
                                else {
                                    out.write("------------" + name03 + "の武将情報--------------");
                                }
                                out.newLine();
                                ckw02 = 1;
                            }

                            if (writecount == 1)
                            {
                                out.write("\t\t-" + remove.toString());
                                out.newLine();
                                writecount = 0;
                                if ((n[0] == 1) && (n[1] == 1) && (n[2] == 1) && (n[3] == 1) && (n[4] == 1)
                                        && (n[5] == 1)&& (n[6] == 1)&& (n[7] == 1)&& (n[8] == 1)&& (n[9] == 1)&& (n[10] == 0))
                                {
                                    out.write("--------------ここまで----------------");
                                    out.newLine();
                                    out.newLine();
                                    n[10] = 1;
                                }
                            }
                            if ((this.remove.indexOf("凡例") != -1) && (this.n[0] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[0] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("時代") != -1) && (n[1] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[1] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("生誕") != -1) && (n[2] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[2] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("死没") != -1) && (n[3] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[3] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("改名") != -1) && (n[4] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[4] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("官位") != -1) && (n[5] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[5] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("主君") != -1) && (n[6] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[6] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("氏族") != -1) && (n[7] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[7] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("父母") != -1) && (n[8] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[8] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("兄弟") != -1) && (n[9] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[9] = 1;
                                writecount += 1;
                            }

                            out.close();
                        }

                        catch (Exception e) {}
                    }
                }
                ckw01.CheckWord();
            }//ここまで

            //4人目
            if ((content04 instanceof InputStream))
            {
                BufferedReader bf = new BufferedReader(new InputStreamReader
                        ((InputStream) content04, "utf-8"));//文字コード

                while ((strLine = bf.readLine()) != null) {
                    Log.d("ログ", strLine); //ソースコード確認
                    remove = HtmlTagRemover1(strLine);

                    if (remove != "\n") {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));
                            if (ckw02 == 0)
                            {
                                if(name04.equals(check)){
                                    out.write("------------取得失敗--------------");
                                    out.newLine();
                                    out.write("武将名を記述してくだい");
                                    out.newLine();
                                    out.write("---------------------------------");
                                    out.newLine();
                                    out.newLine();
                                }
                                else {
                                    out.write("------------" + name04 + "の武将情報--------------");
                                }
                                out.newLine();
                                ckw02 = 1;
                            }

                            if (writecount == 1)
                            {
                                out.write("\t\t-" + remove.toString());
                                out.newLine();
                                writecount = 0;
                                if ((n[0] == 1) && (n[1] == 1) && (n[2] == 1) && (n[3] == 1) && (n[4] == 1)
                                        && (n[5] == 1)&& (n[6] == 1)&& (n[7] == 1)&& (n[8] == 1)&& (n[9] == 1)&& (n[10] == 0))
                                {
                                    out.write("--------------ここまで----------------");
                                    out.newLine();
                                    out.newLine();
                                    n[10] = 1;
                                }
                            }
                            if ((this.remove.indexOf("凡例") != -1) && (this.n[0] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[0] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("時代") != -1) && (n[1] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[1] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("生誕") != -1) && (n[2] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[2] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("死没") != -1) && (n[3] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[3] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("改名") != -1) && (n[4] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[4] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("官位") != -1) && (n[5] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[5] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("主君") != -1) && (n[6] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[6] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("氏族") != -1) && (n[7] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[7] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("父母") != -1) && (n[8] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[8] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("兄弟") != -1) && (n[9] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[9] = 1;
                                writecount += 1;
                            }

                            out.close();
                        }
                        catch (Exception e) {}
                    }
                }
                ckw01.CheckWord();
            }//ここまで

            //5人目
            if ((content05 instanceof InputStream))
            {
                BufferedReader bf = new BufferedReader(new InputStreamReader
                        ((InputStream) content05, "utf-8"));//文字コード

                while ((strLine = bf.readLine()) != null) {
                    Log.d("ログ", strLine); //ソースコード確認
                    remove = HtmlTagRemover1(strLine);

                    if (remove != "\n") {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));
                            if (ckw02 == 0) {
                                if(name05.equals(check)){
                                    out.write("------------取得失敗--------------");
                                    out.newLine();
                                    out.write("武将名を記述してくだい");
                                    out.newLine();
                                    out.write("---------------------------------");
                                    out.newLine();
                                    out.newLine();
                                }
                                else {
                                    out.write("------------" + name05 + "の武将情報--------------");
                                }
                                out.newLine();
                                ckw02 = 1;
                            }

                            if (writecount == 1)
                            {
                                out.write("\t\t-" + remove.toString());
                                out.newLine();
                                writecount = 0;
                                if ((n[0] == 1) && (n[1] == 1) && (n[2] == 1) && (n[3] == 1) && (n[4] == 1)
                                        && (n[5] == 1)&& (n[6] == 1)&& (n[7] == 1)&& (n[8] == 1)&& (n[9] == 1)&& (n[10] == 0))
                                {
                                    out.write("--------------ここまで----------------");
                                    out.newLine();
                                    out.newLine();
                                    n[10] = 1;
                                }
                            }
                            if ((this.remove.indexOf("凡例") != -1) && (this.n[0] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[0] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("時代") != -1) && (n[1] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[1] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("生誕") != -1) && (n[2] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[2] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("死没") != -1) && (n[3] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[3] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("改名") != -1) && (n[4] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[4] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("官位") != -1) && (n[5] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[5] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("主君") != -1) && (n[6] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[6] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("氏族") != -1) && (n[7] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[7] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("父母") != -1) && (n[8] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[8] = 1;
                                writecount += 1;
                            }
                            if ((remove.indexOf("兄弟") != -1) && (n[9] == 0))
                            {
                                out.write("・" + remove.toString());
                                out.newLine();
                                n[9] = 1;
                                writecount += 1;
                            }

                            out.close();
                        }
                        catch (Exception e) {}
                    }
                }
                ckw01.CheckWord();
            }//ここまで

        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("引数にURLを指定してください");
            System.exit(-1);
        }
        catch (IOException e) {
            System.err.println(e);
            System.exit(-1);
        }
        catch(Exception e){
        }
        finish();
    }
    class CheckWord {
        CheckWord() {
        }

        void CheckWord() {
            n[0] = 0;
            n[1] = 0;
            n[2] = 0;
            n[3] = 0;
            n[4] = 0;
            n[5] = 0;
            n[6] = 0;
            n[7] = 0;
            n[8] = 0;
            n[9] = 0;
            n[10]=0;
            n[11]=0;
            ckw02 = 0;
        }
    }

    public static String HtmlTagRemover1(String str) {
        // 文字列のすべてのタグを取り除く
        str = str.replaceAll("<.+?>", "");
        str = str.replaceAll("<title>.+?</title>", "");
        str = str.replaceAll("document.+?=", "");
        str = str.replaceAll("document.+?/", "");
        str = str.replaceAll("window.+?", "");
        str = str.replaceAll("function...+?", "");
        str = str.replaceAll("RLQ.+?", "");
        str = str.replaceAll("push.+?", "");
        str = str.replaceAll("target.+?", "");
        str = str.replaceAll("mw+?", "");
        str = str.replaceAll("mobile+?", "");
        str = str.replaceAll(".+?;", "");
        str = str.replaceAll(".+?/>", "");
        str = str.replaceAll("\n", "");
        str = str.replaceAll("charset.+?>", "");
        str = str.replaceAll("\\}", "");
        str = str.replaceAll("<!--", "");
        str = str.replaceAll("-->", "");
        str = str.replaceAll("\">", "・");
        //str = str.replaceAll("”>", "");
        str = str.replaceAll("var", "");
        str = str.replaceAll("if", "");
        return str.replaceAll("\\/#", "");

    }
}