package com.c0113212.getbusyoinformation;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

/**
 * Created by Kmac on 16/03/11.
 */


public class FirstDescriptionActivity extends Activity{

    private static final int DestroyDescription = 114514;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_description_activity);

        Button finish01 = (Button)findViewById(R.id.rb_finish);
        finish01.setOnClickListener(new FinishClickListener());

//        ちぇっくぼたんをおした時の処理
        final CheckBox checkBox = (CheckBox) findViewById(R.id.rb_description);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//データ保存の書き込み（チェックをおした時）
                    SharedPreferences dataStore = getSharedPreferences("FirstDescription", MODE_PRIVATE);
                    SharedPreferences.Editor editor = dataStore.edit();
                    // Key: input, value: text
                    editor.putInt("input", 114514);
                    Log.d("チェックボタン", "押したのんな");
                    editor.commit();
                } else {
//データ保存の書き込み（チェックを外したとき）
                    SharedPreferences dataStore = getSharedPreferences("FirstDescription", MODE_PRIVATE);
                    SharedPreferences.Editor editor = dataStore.edit();
                    // Key: input, value: text
                    editor.putInt("input", 0);
                    Log.d("チェックボタン","押してないのんな");
                    editor.commit();
                }
            }
        });
    }

//    アクティビティーの終了
    class FinishClickListener implements View.OnClickListener {
        public void onClick(View v) {
            finish();
        }
    }
}
