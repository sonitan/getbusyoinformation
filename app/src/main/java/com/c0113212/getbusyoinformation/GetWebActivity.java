package com.c0113212.getbusyoinformation;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class GetWebActivity extends Activity{

    private int count=0; //秒数カウント
    private int coun02=0;//onstartの呼び出しを一回にする設定
    private static final String FILE_NAME = "FileSampleFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_web);


//        HTMLソースコードを取得するボタン
        Button sendurl01 = (Button)findViewById(R.id.sendwiki);
        sendurl01.setOnClickListener(new SendUrlClickListener02());
        // 表示ボタンのクリックリスナー設定
        Button readBtn = (Button)findViewById(R.id.get_html);
        readBtn.setTag("display");
        readBtn.setOnClickListener(new ButtonClickListener());

        // 削除ボタンのクリックリスナー設定
        Button delBtn = (Button)findViewById(R.id.clear_html);
        delBtn.setTag("delete");
        delBtn.setOnClickListener(new ButtonClickListener());
    }


    class ButtonClickListener implements OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v){
            // タグの取得
            String tag = (String)v.getTag();

            // メッセージ表示用
            String str  = "";
            TextView label = (TextView)findViewById(R.id.tv_message);

            if(tag.equals("display")){

                // ファイルからデータ取得
                try{
                    FileInputStream stream
                            = openFileInput(FILE_NAME);
                    BufferedReader in
                            = new BufferedReader(new InputStreamReader(stream));

                    String line = "";
                    while((line = in.readLine())!=null){
                        str += line + "\n";
                    }
                    in.close();
                }catch(Exception e){
                    str  = "データ取得に失敗しました！";
                }

                // 削除ボタンが押された場合

            }else if(tag.endsWith("delete")){

                // ファイルのデータ削除
                try{
                    deleteFile(FILE_NAME);
                    str = "削除しました！";
                }catch(Exception e){
                    str  = "データ削除に失敗しました！";
                }
            }
            // メッセージ表示
            label.setText(str);
        }
    }

    class SendUrlClickListener02 extends Activity implements OnClickListener {
        public void onClick(View v) {
            GetWeb gtwb2 = new GetWeb();
            gtwb2.GetWeb();
        }
    }

    class GetWeb{

        void GetWeb(){
            //名前の取得
            EditText gethtml01 = (EditText)findViewById(R.id.wiki01);
            EditText gethtml02 = (EditText)findViewById(R.id.wiki02);
            EditText gethtml03 = (EditText)findViewById(R.id.wiki03);
            EditText gethtml04 = (EditText)findViewById(R.id.wiki04);
            EditText gethtml05 = (EditText)findViewById(R.id.wiki05);
            //GetWebServiceに送る武将名前
            String getname01 =gethtml01.getText().toString();
            String getname02 =gethtml02.getText().toString();
            String getname03 =gethtml03.getText().toString();
            String getname04 =gethtml04.getText().toString();
            String getname05 =gethtml05.getText().toString();

            String wikipediaURL="https://ja.wikipedia.org/wiki/";
            String getwiki01 =gethtml01.getText().toString();
            String getwiki02 =gethtml02.getText().toString();
            String getwiki03 =gethtml03.getText().toString();
            String getwiki04 =gethtml04.getText().toString();
            String getwiki05 =gethtml05.getText().toString();

            //URL作成
            StringBuffer buf01 = new StringBuffer();
            buf01.append(wikipediaURL);
            buf01.append(getwiki01);
            String wiki01 = buf01.toString();

            StringBuffer buf02 = new StringBuffer();
            buf02.append(wikipediaURL);
            buf02.append(getwiki02);
            String wiki02 = buf02.toString();

            StringBuffer buf03 = new StringBuffer();
            buf03.append(wikipediaURL);
            buf03.append(getwiki03);
            String wiki03 = buf03.toString();

            StringBuffer buf04 = new StringBuffer();
            buf04.append(wikipediaURL);
            buf04.append(getwiki04);
            String wiki04 = buf04.toString();

            StringBuffer buf05 = new StringBuffer();
            buf05.append(wikipediaURL);
            buf05.append(getwiki05);
            String wiki05 = buf05.toString();

            //GetWebSerViceへのintent作成及び情報付加
            Intent intent= new Intent(GetWebActivity.this,GetWebService.class);//SecondActivityへのintent定義
            intent.putExtra("GETURL01",wiki01);
            intent.putExtra("GETURL02",wiki02);
            intent.putExtra("GETURL03",wiki03);
            intent.putExtra("GETURL04",wiki04);
            intent.putExtra("GETURL05",wiki05);

            intent.putExtra("GETNAME01",getname01);
            intent.putExtra("GETNAME02",getname02);
            intent.putExtra("GETNAME03",getname03);
            intent.putExtra("GETNAME04",getname04);
            intent.putExtra("GETNAME05",getname05);
////SecondActivityの呼び出し
            startActivity(intent);
        }
    }



    @Override
    protected void onPause() {
        // TODO 自動生成されたメソッド・スタブ
        super.onPause();
        TextView chek01 =(TextView)findViewById(R.id.tx_html);
        chek01.setText("Html取得中");
        count=1;
    }


    @Override
    protected  void onStart(){
        super.onStart();

        SharedPreferences dataStore = getSharedPreferences("FirstDescription", MODE_PRIVATE);
        int description01 = dataStore.getInt("input", 0);
        if(coun02 ==0){//画面を横にした時に再び起動しないように
            Log.d("説明チェック","最初の表示");
            if(description01 ==0) {//0の時は説明が出るFirstDescriptionで説明をいらないにチェックすると114514が変数に入る
                Intent intent = new Intent(GetWebActivity.this, FirstDescriptionActivity.class);
                coun02 =1;
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO 自動生成されたメソッド・スタブ
        super.onResume();
        TextView chek01 =(TextView)findViewById(R.id.tx_html);
        if(count ==0){
            count=1;
        }
        else {
            chek01.setText("情報取得完了");
            count=0;
        }

    }
}